#include <scrutiny/scrutiny.h>

void *
procSetup(void *global_ctx);
void
procCleanup(void *group_ctx);
void
findSelf(void);
void
getSelfPath(void);
void
iterateProcs(void);

void *
fdSetup(void *global_ctx);
void
fdCleanup(void *group_ctx);
void
iterateFds(void);

void
serverCleanup(void *group_ctx);

void *
ipv4TcpServerSetup(void *global_ctx);
void
ipv4TcpFindServer(void);
void
ipv4TcpFindClient(void);

void *
ipv6TcpServerSetup(void *global_ctx);
void
ipv6TcpFindServer(void);
void
ipv6TcpFindClient(void);

void *
ipv4UdpServerSetup(void *global_ctx);
void
ipv4UdpFindServer(void);

void *
ipv6UdpServerSetup(void *global_ctx);
void
ipv6UdpFindServer(void);

void *
domainServerSetup(void *global_ctx);
void
domainServerCleanup(void *group_ctx);
void
domainFindServer(void);

void
iterateThreads(void);
void
iterateMaps(void);

int
main()
{
    scrGroup group;
    const scrTestOptions timeout_options = {.timeout = 5};

    group = scrGroupCreate(procSetup, procCleanup);
    scrGroupAddTest(group, "Get proc info", findSelf, NULL);
    scrGroupAddTest(group, "Get self path", getSelfPath, NULL);
    scrGroupAddTest(group, "Proc iterator", iterateProcs, &timeout_options);

    group = scrGroupCreate(fdSetup, fdCleanup);
    scrGroupAddTest(group, "File descriptor iterator", iterateFds, &timeout_options);

    group = scrGroupCreate(ipv4TcpServerSetup, serverCleanup);
    scrGroupAddTest(group, "Net iterator find IPv4 TCP server", ipv4TcpFindServer, &timeout_options);
    scrGroupAddTest(group, "Net iterator find IPv4 TCP client", ipv4TcpFindClient, &timeout_options);

    group = scrGroupCreate(ipv6TcpServerSetup, serverCleanup);
    scrGroupAddTest(group, "Net iterator find IPv6 TCP server", ipv6TcpFindServer, &timeout_options);
    scrGroupAddTest(group, "Net iterator find IPv6 TCP client", ipv6TcpFindClient, &timeout_options);

    group = scrGroupCreate(ipv4UdpServerSetup, serverCleanup);
    scrGroupAddTest(group, "Net iterator find IPv4 UDP server", ipv4UdpFindServer, &timeout_options);

    group = scrGroupCreate(ipv6UdpServerSetup, serverCleanup);
    scrGroupAddTest(group, "Net iterator find IPv6 UDP server", ipv6UdpFindServer, &timeout_options);

    group = scrGroupCreate(domainServerSetup, domainServerCleanup);
    scrGroupAddTest(group, "Domain socket iterator find server", domainFindServer, &timeout_options);

    group = scrGroupCreate(NULL, NULL);
    scrGroupAddTest(group, "Thread iterator", iterateThreads, &timeout_options);
    scrGroupAddTest(group, "Map iterator", iterateMaps, &timeout_options);

    return scrRun(NULL, NULL);
}
