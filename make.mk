ifndef REAP_MK
REAP_MK :=

REAP_LIB_DIR ?= $(REAP_DIR)
REAP_OBJ_DIR ?= $(REAP_DIR)/src

REAP_SHARED_LIBRARY := $(REAP_LIB_DIR)/libreap.so
REAP_STATIC_LIBRARY := $(REAP_LIB_DIR)/libreap.a

REAP_SOURCE_FILES := $(wildcard $(REAP_DIR)/src/*.c)
REAP_OBJECT_FILES := $(patsubst $(REAP_DIR)/src/%.c,$(REAP_OBJ_DIR)/%.o,$(REAP_SOURCE_FILES))
REAP_HEADER_FILES := $(wildcard $(REAP_DIR)/include/reap/*.h)
REAP_INCLUDE_FLAGS := -I$(REAP_DIR)/include

$(REAP_SHARED_LIBRARY): $(REAP_OBJECT_FILES) | $(REAP_LIB_DIR)
	$(CC) $(LDFLAGS) -shared -o $@ $^

$(REAP_STATIC_LIBRARY): $(REAP_OBJECT_FILES) | $(REAP_LIB_DIR)
	$(AR) rcs $@ $^

$(REAP_OBJ_DIR)/%.o: $(REAP_DIR)/src/%.c $(REAP_HEADER_FILES)
	$(CC) $(CFLAGS) $(REAP_INCLUDE_FLAGS) -fPIC -c $< -o $@

$(REAP_LIB_DIR) $(REAP_OBJ_DIR):
	@mkdir -p $@

reap_clean:
	@rm -f $(REAP_SHARED_LIBRARY) $(REAP_STATIC_LIBRARY) $(REAP_OBJECT_FILES)

.PHONY: reap_clean

CLEAN_TARGETS += reap_clean

endif
