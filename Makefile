CFLAGS := -std=gnu11 -fdiagnostics-color -Wall -Wextra -Werror -fvisibility=hidden
ifeq ($(debug),yes)
    CFLAGS += -O0 -g -DDEBUG
else
    CFLAGS += -O2 -DNDEBUG
endif

all: _all

REAP_DIR := .
include make.mk

TEST_DIR := tests
include tests/make.mk

.PHONY: all _all tests format clean

_all: $(REAP_SHARED_LIBRARY) $(REAP_STATIC_LIBRARY)

format:
	find . -name '*.[hc]' -print0 | xargs -0 -n 1 clang-format -i

clean: $(CLEAN_TARGETS)
